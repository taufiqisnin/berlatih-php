<?php

require './animal.php';
require './Ape.php';
require './Frog.php';

$sheep = new Animal("shaun");

echo $sheep->name.'<br>'; // "shaun"
echo $sheep->legs.'<br>'; // 2
echo $sheep->cold_blooded().'<br><br>'; // false

$sungokong = new Ape("kera sakti");
echo $sungokong->name.'<br>'; // "kera sakti"
echo $sungokong->legs.'<br>'; // 2
echo $sungokong->cold_blooded().'<br>'; // false
echo $sungokong->yell().'<br><br>'; // "Auooo"

$kodok = new Frog("buduk");
echo $kodok->name.'<br>'; // "buduk"
echo $kodok->legs.'<br>'; // 4
echo $sungokong->cold_blooded().'<br>'; // false
echo $kodok->jump() ; // "hop hop"