<?php

require_once './animal.php';

class Frog extends animal
{
	public $jump = "hop hop";
	public $legs = 4;

	public function jump() {
		return $this->jump;
	}
} 

?>